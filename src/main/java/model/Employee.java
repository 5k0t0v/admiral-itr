package model;

import Service.EmployeeService;
import lombok.Data;

@Data
public class Employee {
    private String name;

    public Employee(String name) {
        this.name = name;
        EmployeeService.getInstance().getEmployees().add(this);
    }

    @Override
    public String toString() {
        return String.format("name: %s", this.name);
    }
}
