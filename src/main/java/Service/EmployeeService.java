package Service;

import lombok.Data;
import model.Employee;

import java.util.HashSet;
import java.util.Set;

@Data
public class EmployeeService {
    public static EmployeeService instance;

    private Set<Employee> employees;

    private EmployeeService(Set<Employee> employees) {
        this.employees = employees;
    }

    public static synchronized EmployeeService getInstance() {
        if (instance == null)
            instance = new EmployeeService(new HashSet<>());
        return instance;
    }

    public Employee getEmployee(String name) {
        return employees.stream().filter(e ->
                e.getName().equals(name)).
                findFirst().
                orElse(new Employee(name));
    }

}
