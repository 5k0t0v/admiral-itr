package Service;

import lombok.Data;
import model.Employee;
import model.TimeInterval;

import java.util.*;

@Data
public class TimeIntervalsService {
    public static TimeIntervalsService instance;
    private final Map<Employee, List<TimeInterval>> employeeTimeIntervals;


    private TimeIntervalsService(Map<Employee, List<TimeInterval>> employeeTimeIntervals) {
        this.employeeTimeIntervals = employeeTimeIntervals;
    }

    public static synchronized TimeIntervalsService getInstance() {
        if (instance == null)
            instance = new TimeIntervalsService(
                    new HashMap<>());
        return instance;
    }

    public List<TimeInterval> getTimeIntervals(Employee employee) {
        if (employeeTimeIntervals.isEmpty()) {
            employeeTimeIntervals.put(employee, new ArrayList<>());
        }
        employeeTimeIntervals.computeIfAbsent(employee, k -> new ArrayList<>());
        return employeeTimeIntervals.get(employee);
    }
}
